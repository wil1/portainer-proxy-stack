#!/bin/bash
set -eux
STACKNAME=portainer
DATESTAMP=$(date +"%Y%m%d_%H%M%S")
BACKUPDIR=./_backups/${STACKNAME}-${DATESTAMP}

mkdir -p ${BACKUPDIR}

backup() {
    VOLUME=$1
    docker run -v ${STACKNAME}_${VOLUME}:/volume --rm loomchild/volume-backup backup - > ${BACKUPDIR}/${VOLUME}.tar.bz2
}

backup certs
backup html
backup portainer-data
backup vhost.d