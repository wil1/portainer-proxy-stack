#!/bin/bash

# 1) check if secret exists
[ ! "$(docker secret ls | grep 'global_admin_password')" ] \
    && echo "
Global admin password secret required:
    echo \"MySecretPassword\" | docker secret create global_admin_password -" \
    && exit 1
    
# 2) create network
[ ! "$(docker network ls | grep 'nginx_proxy')" ] \
    && docker network create -d overlay nginx_proxy

# 3) merge docker-compose.yml and .env file (docker-compose config), and deploy it to docker swarm as a stack
# (docker-compose config) - https://github.com/moby/moby/issues/29133#issuecomment-442912378
docker stack deploy -c <(docker-compose config) portainer
