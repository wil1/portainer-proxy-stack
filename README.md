# Installation

1. Create a docker swarm secret called "global_admin_password". This will be used as the Portainer password.
    `echo "MySecretPassword" | docker secret create global_admin_password -`
2. Copy .env-example to .env file and set the base domain and LetsEncrypt email address
3. Run ./start-portainer.sh